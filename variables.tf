# variables.tf

variable "DOCKER_REGISTRY" {
  default = "115554601907.dkr.ecr.us-east-2.amazonaws.com"
}

variable "ECS_CLUSTER" {
  default = "c-sharp-cluster"
}

variable "ECS_SERVICE" {
  default = "c-sharp-ecs-service"
}

variable "ECS_TASK_FAMILY" {
  default = "c-sharp-ecs-task-family"
}

variable "ECS_CONTAINER_NAME" {
  default = "c-sharp-app-container"
}

variable "AWS_DEFAULT_REGION" {
  default = "us-east-2"
}

variable "docker_registry" {
  description = "Docker registry address"
}

variable "ecs_cluster" {
  description = "ECS cluster name"
}

variable "ecs_service" {
  description = "ECS service name"
}

variable "ecs_task_family" {
  description = "ECS task family"
}

variable "ecs_container_name" {
  description = "ECS container name"
}

variable "aws_default_region" {
  description = "AWS default region"
}
